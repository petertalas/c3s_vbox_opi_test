import string

# Files
list = open("./deviceList.csv", "r")
outputFile = open("./output.bob", "w")

# Statics
Device = '{Device}'
WIDSecSub = '{WIDSecSub}'
WIDDis = '{WIDDis}'
PV = '{PV}'

# Variables
DisplayName = 'C3S Devices'
LabelText = 'C3S - Devices'
varWIDDis = 'PLC'
varWIDSecSub = 'Tgt-1047'
titleWidth = '2550'
breakPoint = 30

# start
y = 105

outputFile.write(F'''<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>{DisplayName}</name>
  <width>{titleWidth}</width>
  <height>130</height>
  <widget type="group" version="2.0.0">
    <name>TitleBar</name>
    <width>{titleWidth}</width>
    <height>50</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle</name>
      <class>TITLE-BAR</class>
      <x use_class="true">0</x>
      <y use_class="true">0</y>
      <width>{titleWidth}</width>
      <height use_class="true">50</height>
      <line_width use_class="true">0</line_width>
      <background_color use_class="true">
        <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Title</name>
      <text>{LabelText}</text>
      <class>TITLE</class>
      <x use_class="true">20</x>
      <y use_class="true">0</y>
      <width>600</width>
      <height use_class="true">50</height>
      <font use_class="true">
        <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="HEADER-TEXT" red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <transparent use_class="true">true</transparent>
      <horizontal_alignment use_class="true">0</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
      <wrap_words use_class="true">false</wrap_words>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>TableTitle</name>
    <x>0</x>
    <y>70</y>
    <width>480</width>
    <height>30</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>TblTitle</name>
      <width>480</width>
      <height>30</height>
      <line_width>1</line_width>
      <line_color>
        <color name="BLACK-BORDER" red="121" green="121" blue="121">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col1</name>
      <text>VBX</text>
      <width>40</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col2</name>
      <text>4.5K Supply</text>
      <x>40</x>
      <width>130</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col3</name>
      <text>Command</text>
      <x>180</x>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col4</name>
      <text>Reedback</text>
      <x>300</x>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col5</name>
      <text>Face Plate</text>
      <x>400</x>
      <width>80</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>TableTitle2</name>
    <x>500</x>
    <y>70</y>
    <width>360</width>
    <height>30</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>TblTitle</name>
      <width>360</width>
      <height>30</height>
      <line_width>1</line_width>
      <line_color>
        <color name="BLACK-BORDER" red="121" green="121" blue="121">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col1</name>
      <text>VBX</text>
      <width>40</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col2</name>
      <text>4.5K Supply</text>
      <x>40</x>
      <width>130</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col3</name>
      <text>Value</text>
      <x>180</x>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>col4</name>
      <text>Face Plate</text>
      <x>280</x>
      <width>80</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>''')

x1 = 10
x2 = 40
x3 = 300
x4 = 180
x5 = 420

i = 1

# MainCycle
for aline in list:
    values = aline.split(",")
    count = int(values[0])
    vbxNr = int(values[1])
    vbxTag = values[2]
    vbxPv = values[3]
    pvDev = "${mDev}"
    pvDis = values[5]
    pvIndex = "${mIndex}"
    pvSecSub = values[7]

    Nr = "vbxNr" + str(count)
    Tag = "Tag" + str(count)
    Rdb = "Rdb" + str(count)
    cvCmd = "cvCmd" + str(count)
    btnOpen = "btnOpen"+str(count)

    outputFile.write(F'''
  <widget type="label" version="2.0.0">
    <name>{Nr}</name>
    <text>{vbxNr}</text>
    <x>{x1}</x>
    <y>{y}</y>
    <width>20</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>{Tag}</name>
    <text>{vbxTag}</text>
    <x>{x2}</x>
    <y>{y}</y>
    <width>130</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>{Rdb}</name>
    <pv_name>{vbxPv+':ValvePosition'}</pv_name>
    <x>{x3}</x>
    <y>{y}</y>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>{cvCmd}</name>
    <pv_name>{vbxPv+':ValveSP'}</pv_name>
    <x>{x4}</x>
    <y>{y}</y>
  </widget>
  <widget type="action_button" version="3.0.0">
  <name>btnOpen</name>
  <actions>
    <action type="open_display">
      <file>99-Shared/Valves/valve analog/faceplates/CV_ControlVALVE_Faceplate_OnlyValve.bob</file>
      <macros>
        <Dev>{pvDev}</Dev>
        <Dis>{pvDis}</Dis>
        <Index>{pvIndex}</Index>
        <SecSub>{pvSecSub}</SecSub>
      </macros>
      <target>window</target>
      <description>Open Display</description>
    </action>
  </actions>
  <text>Open</text>
  <x>{x5}</x>
  <y>{y}</y>
  <width>50</width>
  <height>20</height>
  <tooltip>$(actions)</tooltip>
</widget>

''')
    y += 35
    i += 1
    if i == breakPoint+1:
        break

y = 105
x1 = x1 + 500
x2 = x2 + 500
x3 = x3 + 500
x4 = x4 + 500
x5 = x5 + 500

for aline in list:
    values = aline.split(",")
    count = int(values[0])
    vbxNr = int(values[1])
    vbxTag = values[2]
    vbxPv = values[3]
    Nr = "vbxNr" + str(count)
    Tag = "Tag" + str(count)
    Rdb = "Rdb" + str(count)
    pvDev = "${mDev}"
    pvDis = values[5]
    pvIndex = "${mIndex}"
    pvSecSub = values[7]

    outputFile.write(F'''
  <widget type="label" version="2.0.0">
    <name>{Nr}</name>
    <text>{vbxNr}</text>
    <x>{x1}</x>
    <y>{y}</y>
    <width>20</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>{Tag}</name>
    <text>{vbxTag}</text>
    <x>{x2}</x>
    <y>{y}</y>
    <width>130</width>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>{Rdb}</name>
    <pv_name>{vbxPv+':MeasValue'}</pv_name>
    <x>{x4}</x>
    <y>{y}</y>
  </widget>
  <widget type="action_button" version="3.0.0">
  <name>btnOpen</name>
  <actions>
    <action type="open_display">
      <file>99-Shared/AnalogTransmitters/faceplates/AnalogTransmitter_Faceplate.bob</file>
      <macros>
        <Dev>{pvDev}</Dev>
        <Dis>{pvDis}</Dis>
        <Index>{pvIndex}</Index>
        <SecSub>{pvSecSub}</SecSub>
      </macros>
      <target>window</target>
      <description>Open Display</description>
    </action>
  </actions>
  <text>Open</text>
  <x>{x3}</x>
  <y>{y}</y>
  <width>50</width>
  <height>20</height>
  <tooltip>$(actions)</tooltip>
  </widget>''')
    y += 35
    i += 1

outputFile.write('''
</display>
''')
outputFile.close()

print("Job done!")
